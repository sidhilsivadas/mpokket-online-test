<?php

namespace App\Repositories;

use App\Http\Controllers\TestController;
use App\Todo;

class TodoRepository
{

    public static function saveTodo($formData){
    	$todo = new Todo;
    	$todo->user_id = $formData["data"]["userId"];
        $todo->title = $formData["data"]["title"];
        $todo->notes = $formData["data"]["notes"];
        $todo->dueDate = $formData["data"]["dueDate"];
        //$todo->status = $formData["data"]["status"];
        $todo->save();
    }


    public static function updateTodo($formData,$id){
    	$todo = Todo::find($id);
    	$todo->user_id = $formData["data"]["userId"];
        $todo->title = $formData["data"]["title"];
        $todo->notes = $formData["data"]["notes"];
        $todo->dueDate = $formData["data"]["dueDate"];
        $todo->status = $formData["data"]["status"];
        $todo->save();
    }

    public static function getAllTodos($id){
    	$todo = Todo::where('user_id', $id)->get();
    	return $todo;
    }

    
}
