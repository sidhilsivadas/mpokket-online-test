<?php

namespace App\Services;

use App\Http\Controllers\TodoController;
use App\Repositories\TodoRepository;
use GuzzleHttp\Client as HttpClient;

class TodoService
{

    public static function saveTodo($formData){
        $data = TodoRepository::saveTodo($formData); 
        return $data;
    }

    public static function updateTodo($formData,$id){
        $data = TodoRepository::updateTodo($formData,$id);      
        return $data;
    }

    public static function getAllTodos($userId){
        $data = TodoRepository::getAllTodos($id);      
        return $data;
    }

    
}
