<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Services\TestService;

class TestController extends Controller
{

    private $statusCode = 200;

    function index(Request $request){
    	$data = TestService::getTestData();
    	$formData = $request->all();
    	$this->statusCode = 201;
    	return $this->respond([
            'status' => 'error',
            'status_code' => 200,
            'message' => $data,
        ]);
    }

    public function respond($data, $headers = []){
        return Response::json($data, $this->statusCode, $headers);
    }
}
