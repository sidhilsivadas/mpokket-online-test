<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodolistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todolist', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->longText('notes');
            $table->date('duedate')->nullable()->default(NULL);
            $table->enum('status', ['created','updated','completed'])->default("created");
            $table->timestamps();           

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todolist');
    }
}
